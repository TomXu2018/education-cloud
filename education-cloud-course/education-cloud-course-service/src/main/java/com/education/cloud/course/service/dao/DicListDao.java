package com.education.cloud.course.service.dao;

import com.education.cloud.course.service.dao.impl.mapper.entity.DicList;
import com.education.cloud.course.service.dao.impl.mapper.entity.DicListExample;
import com.education.cloud.util.base.Page;

public interface DicListDao {
    int save(DicList record);

    int deleteById(Long id);

    int updateById(DicList record);

    DicList getById(Long id);

    Page<DicList> listForPage(int pageCurrent, int pageSize, DicListExample example);
}

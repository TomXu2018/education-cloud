package com.education.cloud.system.service.api.pc.biz;

import com.education.cloud.system.common.req.WebsiteUpdateREQ;
import com.education.cloud.system.common.resq.WebsiteViewRESQ;
import com.education.cloud.system.service.dao.WebsiteDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.education.cloud.system.service.dao.impl.mapper.entity.Website;
import com.education.cloud.util.base.Page;
import com.education.cloud.util.base.PageUtil;
import com.education.cloud.util.base.Result;
import com.education.cloud.util.enums.ResultEnum;
import com.education.cloud.util.tools.BeanUtil;
import cn.hutool.core.util.ObjectUtil;

/**
 * 站点信息
 *
 */
@Component
public class PcApiWebsiteBiz {

	@Autowired
	private WebsiteDao dao;

	public Result<WebsiteViewRESQ> view() {
		Website website = dao.getWebsite();
		return Result.success(BeanUtil.copyProperties(website, WebsiteViewRESQ.class));
	}

	public Result<Integer> update(WebsiteUpdateREQ req) {
		Website record = BeanUtil.copyProperties(req, Website.class);
		int results = dao.updateById(record);
		if (results > 0) {
			return Result.success(results);
		}
		return Result.error(ResultEnum.SYSTEM_SAVE_FAIL);
	}

}
